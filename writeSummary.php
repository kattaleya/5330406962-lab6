<?php
	$doc = new DOMDocument;
        $dom = new DomDocument('1.0','UTF-8'); 
        $doc->load('http://www.gotoknow.org/blogs/posts?format=rss');

        $root = $doc->documentElement;
        $loc = $root->getElementsByTagName('item');

        echo "Reading from summaryBlogs.xml<br>";
        $items = $dom->appendChild($dom->createElement('items'));

        foreach ($loc AS $item) {
                $title1 = $item->getElementsByTagName("title")->item(0)->nodeValue;
                $link1 = $item->getElementsByTagName("link")->item(0)->nodeValue;
                $author1 = $item->getElementsByTagName("author")->item(0)->nodeValue;
                
                $item = $items->appendChild($dom->createElement('item')); 

                $title2 = $item->appendChild($dom->createElement('title')); 
                $title2->appendChild( $dom->createTextNode($title1));
                $link2 = $item->appendChild($dom->createElement('link'));
                $link2->appendChild( $dom->createTextNode($link1));
                $author2 = $item->appendChild($dom->createElement('author')); 
                $author2->appendChild( $dom->createTextNode($author1));

                echo $link1 . $author1 ." ";
        }

        $dom->formatOutput = true;
        $test1 = $dom->saveXML();
        $dom->save('summaryBlogs.xml'); 

?>